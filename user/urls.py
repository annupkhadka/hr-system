from django.urls import path

from .views import *

app_name = "user"

urlpatterns = [
    path('employee-registration/', EmployeeRegistration.as_view(), name='employee-registration'),
    path('login', LoginPageView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),

    path('home', Homepage.as_view(), name='home' )
]


from django.db import models
import uuid
from leave_tracker.abstract import BaseModel
from user.models import AuthUser


STATUS = [
        ("new", "new"),
        ("approved", "approved"),
        ("self_cancelled", "self cnacelled"),
        ("cancelled", "cancelled")
    ]

LEAVE_REQUEST_TYPE = [
        ("fullday", "Full day"),
        ("firsthalf","First half"),
        ("secondhalf","Second half")
    ]

LEAVE_TYPE = [
        ("annual_leave", "Annual Leave"),
        ("sick_leave", " Sick Leave"),
        ("unpaid_leave", "Unpaid Leave"),
        ("saturday_subtitute_leave", "Saterday Subtitute Leave"),
        ("maternity_leave", "Maternity Leave"),
        ("paternity_leave", "Paternity Leave")
    ]

class LeaveRequest(models.Model):
    user = models.ForeignKey(AuthUser, on_delete=models.CASCADE, related_name='employee_leave_request')
    leave_type = models.CharField(max_length=50, choices=LEAVE_TYPE)
    leave_request_type = models.CharField(max_length=15, choices= LEAVE_REQUEST_TYPE)
    status = models.CharField(max_length=25, choices=STATUS)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()
    request_days = models.PositiveIntegerField()
    contact_info = models.ForeignKey(AuthUser, on_delete=models.SET_NULL, null=True, blank=True)
    description = models.TextField()

    def __str__(self):
        return self.leave_type + '  ' + str(self.request_days)
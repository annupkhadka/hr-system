from re import A
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.auth import authenticate, logout, login
from django.views.generic import View, UpdateView, ListView, DeleteView, CreateView, FormView, TemplateView
from .mixins import CustomLoginRequiredMixin
from .models import *
from .forms import *

class EmployeeRegistration(CreateView):
    template_name = 'user/register.html'
    # success_url = reverse_lazy('user:home')

    def get(self, request,*args, **kwargs):
        print('in gett----')
        user_form = UserForm()
        print('_user form ---', user_form)
        official_detail = EmployeeOfficialDetailForm()
        context ={}
        context['user_form']= user_form
        context['official_detail'] = official_detail
        return render(request, self.template_name, context)
    
    def post(self, request, *args, **kwargs):
        print('000000000000,post')
        user = UserForm(request.POST)
        official_detail = EmployeeOfficialDetailForm(request.POST)
        if user.is_valid():
            print('employee.cleaned_data', user.cleaned_data)

            user= user.save(commit=False)
            if official_detail.is_valid():

                print('----',official_detail, official_detail.cleaned_data)
                user.save()
                EmployeeProfile.objects.create(user=user, **official_detail.cleaned_data, )
                
                return redirect('user:home')

        context = {}
        context['user_form']= user
        context['official_detail'] = official_detail
        return render(request, self.template_name, context)

    ## Login Logout Views
class LoginPageView(FormView):
    form_class = LoginForm
    template_name = "user/login.html"

    def form_valid(self, form):
        print('form.cleaned_data', form.cleaned_data)
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password")
        user = authenticate(email=email, password=password)
        login(self.request, user)

        if "next" in self.request.GET:
            return redirect(self.request.GET.get("next"))
        return redirect("user:home")
    
    def form_invalid(self, form):
        context = {'form':form}
        return render(self.request, self.template_name, context )


class LogoutView(View):

    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect("user:login")
    

class Homepage(TemplateView):
    template_name = 'user/home.html'


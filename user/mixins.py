from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
class FormControlMixin:
    def __init__(self, *args, **kwargs):
        print('------------------------',super())
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class CustomLoginRequiredMixin(LoginRequiredMixin):
    login_url = reverse_lazy('user:login')

    def dispatch(self,request,*args,**kwargs):
        print('custom required mixins')
        if self.request.user.is_authenticated:
            return super().dispatch(request, *args, **kwargs)
        return self.handle_no_permission()
from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import AuthUser, EmployeeProfile
from .mixins import FormControlMixin
from django.core.validators import RegexValidator


class EmployeeOfficialDetailForm(FormControlMixin,forms.ModelForm):

	class Meta:
		model = EmployeeProfile
		fields = ['employee_id', 'designation', 'department']


class UserForm(FormControlMixin,UserCreationForm):

	class Meta:
		model = AuthUser
		fields = ["first_name", "last_name", "mobile_number", "username", "email", "password1","password2"]
		


class LoginForm(FormControlMixin,forms.ModelForm):

	class Meta:
		model = AuthUser
		fields = ["email", "password"]

	def clean(self):
		email = self.cleaned_data.get('email')
		password = self.cleaned_data.get('password')
		user = AuthUser.objects.filter(email=email, is_active=True).first()
		if user == None or not user.check_password(password):
			raise forms.ValidationError(" Incorrect email or password")
		return self.cleaned_data
    


from django import forms

from .models import LeaveRequest
from user.mixins import FormControlMixin


class LeaveRequestForm(FormControlMixin,forms.ModelForm):
    class Meta:
        model = LeaveRequest
        fields = [
            'leave_type', 'leave_request_type', 'from_date',
            'to_date','request_days', 'contact_info', 'description'
        ]
        widgets = {
            'from_date' : forms.DateInput(attrs={'type':'date'}),
            'to_date' : forms.DateInput(attrs={'type': 'date'})
        }

    
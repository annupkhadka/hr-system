from django.urls import path

from .views import *

app_name = "leave_management"

urlpatterns = [
    path('leave-request', LeaveRequestCreateView.as_view(), name='leave-request'),
    path('employee-leave-list/', EmployeeListLeave.as_view(), name='employee-leave-list'),

]


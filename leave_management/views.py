from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.auth import authenticate, logout, login
from django.views.generic import View, UpdateView, ListView, DeleteView, CreateView, FormView, TemplateView
from user.mixins import CustomLoginRequiredMixin
from .models import *
from .forms import *
from user.models import *


class LeaveRequestCreateView(CreateView):
    template_name = 'leave_management/leave_request.html'
    form_class = LeaveRequestForm

    def post(self, request, *args, **kwargs):
        form = LeaveRequestForm(request.POST)
        if form.is_valid():
            print('-------is valid - ----', form.cleaned_data)
            leave_req = form.save(commit=False)
            print('leave_req---', leave_req, leave_req.leave_type)
            leave_req.user = request.user
            leave_req.status = "new"
            leave_req.save()
            print('leave_resssss---', leave_req, leave_req.user)
            return redirect('user:home')

        else:
            return render (request, self.template_name,form)


class EmployeeListLeave(ListView):
    model = LeaveRequest
    template_name = 'leave_management/employee_leave_list.html'
    
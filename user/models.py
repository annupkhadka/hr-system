from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid
from leave_tracker.abstract import BaseModel
# Create your models here.

class AuthUser(AbstractUser):
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField( max_length=150, blank=True)
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    mobile_number = models.CharField(max_length=15, blank=False, unique=True)
    email = models.EmailField(unique=True)
    is_email_verified = models.BooleanField(default=False)
    need_password_change = models.BooleanField(default=False)

    password_change_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(
        blank=False, auto_now_add=True, null=True)
    updated_date = models.DateTimeField(blank=True, auto_now=True, null=True)
    last_active = models.DateTimeField(blank=True, null=True)
    otp = models.IntegerField(null=True,blank=True)
    activation_key = models.CharField(max_length=150,blank=True,null=True)

    created_by = models.ForeignKey(
        "AuthUser",
        on_delete=models.SET_NULL,
        blank=False,
        null=True,
        related_name="user_created_by",
    )
    updated_by = models.ForeignKey(
        "AuthUser",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="user_updated_by",
    )

    # USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email


class Designation(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

class Branch(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)
    is_head_office = models.BooleanField(default = False)

    def __str__(self):
        return self.name

class Department(BaseModel):
    name = models.CharField(max_length=255, null=False, blank=False)
    parent_department = models.ForeignKey(
        "self", related_name="parent_dept", on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.name


GENDER = [
        ("1", 'Male'),
        ("2", 'Female'),
        ("3", 'Others')
    ]
class FamilyInformation(models.Model):
    name = models.CharField(max_length=250, null=True, blank=True)
    relation = models.CharField(max_length=25, null=True, blank=True)
    mobile_number = models.CharField(max_length=10)
    gender = models.CharField(max_length=1, choices=GENDER)


MARITAL_STATUS = [
        ("single", 'Single'),
        ("married", 'Married'),
        ("divorce", 'Divorce'),
        ("others", "others")
    ]
class EmployeeProfile(BaseModel):
    user = models.OneToOneField(AuthUser, on_delete=models.CASCADE, related_name='employee_profile')    
    employee_id = models.CharField(max_length=10, unique=True)
    appointed_date = models.DateTimeField(auto_now_add=True)
    gender = models.CharField(choices=GENDER, max_length=1)
    marital_status = models.CharField(choices=MARITAL_STATUS, max_length=10)
    designation = models.ForeignKey(Designation, on_delete=models.SET_NULL, null=True)
    department = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True)
    branch = models.ForeignKey(Branch, on_delete=models.SET_NULL, null=True)
    blood_group = models.CharField(max_length=4, null=True, blank=True)
    religion = models.CharField(max_length=25)
    date_of_birth = models.DateField(null=True)
    image = models.ImageField(upload_to='user_image', null=True, blank=True) 
    family_members = models.ForeignKey(FamilyInformation, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self) :
        return self.user.email + '  ' + str(self.appointed_date)



